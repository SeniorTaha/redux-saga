import { applyMiddleware, createStore, combineReducers, compose } from 'redux';
import logger from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import { all, fork } from 'redux-saga/effects';

/**import reducers */
import leadLevelsReducer from "./redux/reducers/Lead/levels";

/**import sagas */
import leadSaga from './redux/sagas/lead';


const sagaMiddleware = createSagaMiddleware();

let middleware = process.env.NODE_ENV !== 'production' ? [sagaMiddleware, logger] : [sagaMiddleware];

let reducers = combineReducers({
    lead: combineReducers({
        level: leadLevelsReducer
    }),
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
    reducers,
    process.env.NODE_ENV === 'development' ?
        composeEnhancers(
            applyMiddleware(...middleware)
        )
        :
        applyMiddleware(...middleware)
);

export default store;

function* watchAll() {
    yield all([
        fork(leadSaga),
    ])
}

sagaMiddleware.run(watchAll);