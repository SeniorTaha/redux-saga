import axios from "axios";

export const fetcher = async option => {

    const options = Object.assign(
        {},
        {
          url: undefined,
          method: undefined,
          data: {},
          headers: {},
          formData: false
        },
        option
    );

    if(option.authorization && localStorage.getItem('token')) {
        options.headers.authorization = 'Token ' + localStorage.getItem('token') || '';
    }

    const request = {
        ...options,
        headers: {
            ...options.headers,
            'Content-Type': options.formData
            ? 'multipart/form-data'
            : 'application/json;charset=utf-8'
        }
    }

    try {
        axios.defaults.baseURL = window._env_.REACT_APP_API_URL;

        const response = await axios(request);
        return response.data;
    } catch(err) {
        if (err.response.status === 401) {
            localStorage.removeItem('token');
        }
        throw err.response;
    }
}