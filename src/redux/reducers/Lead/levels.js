import { 
    LEAD_LEVELS,
    LEAD_LEVELS_REQUEST,
    LEAD_LEVELS_SUCCESSFULL,
    LEAD_LEVELS_FAILED
 } from '../../../constants/leadLevel';

const initialState = {
    errorMsg: null,
    hasError: false,
    isSuccess: false,
    isLoading: false,
    data: null
}

export default (state = initialState, action) => {
    switch (action.type) {
        case LEAD_LEVELS_REQUEST: {
            return {
                ...state,
                errorMsg: null,
                hasError: false,
                isSuccess: false,
                isLoading: true
            }
        }
        case LEAD_LEVELS_SUCCESSFULL: {
            return {
                ...state,
                isLoading: false,
                isSuccess: true,
                data: action.payload,
            }
        }
        case LEAD_LEVELS_FAILED: {
            return {
                errorMsg: action.payload,
                hasError: true,
                isSuccess: false,
                isLoading: false,
            }
        }
        case LEAD_LEVELS: {
            return {
                ...state
            }
        }
        default:
            return {
                ...state
            }
    }
}