import { call, put, all, takeLatest } from 'redux-saga/effects';
import { fetcher } from '../../utils/fetcher';

import { leadLevelsSuccessfull, leadLevelsFailure } from '../actions/Lead/levels';
import { LEAD_LEVELS_REQUEST } from '../../constants/leadLevel';


function* leadLevels(action) {
    try {
        const response = yield call(fetcher, {
            url: "url",
            method: 'get',
            params: {
                serviceType: action.payload.serviceType
            },
        });
        yield put(leadLevelsSuccessfull(response));
    } catch (err) {
        yield put(leadLevelsFailure(err));
    }
}

export default function* watchLeadAction() {
    yield all([
        takeLatest(LEAD_LEVELS_REQUEST, leadLevels),
    ])
}