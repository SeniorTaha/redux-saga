import {
    LEAD_LEVELS,
    LEAD_LEVELS_REQUEST,
    LEAD_LEVELS_SUCCESSFULL,
    LEAD_LEVELS_FAILED
} from '../../../constants/leadLevel';

export const leadLevelsRequest = data => ({
    type: LEAD_LEVELS_REQUEST,
    payload: data
});

export const leadLevelsSuccessfull = data => ({
    type: LEAD_LEVELS_SUCCESSFULL,
    payload: data
});

export const leadLevelsFailure = data => ({
    type: LEAD_LEVELS_FAILED,
    payload: data
});

export const leadLevels = data => ({
    type: LEAD_LEVELS,
    payload: data
});